const sanitizeHtml = require('sanitize-html');
const fs = require('fs')

const args = process.argv.slice(2);
const inputFile = args[0];



const cleanOptions = {
	// allowedTags: false, // all tags
	allowedTags: sanitizeHtml.defaults.allowedTags.concat(['h1', 'h2']), // default+
	allowedAttributes: {
		img: ['src', 'alt', 'width', 'height', 'title'],
		a: ['href', 'name', 'target', 'rel'],
	},
	selfClosing: ['img', 'br', 'hr', 'area', 'input', 'meta'],
	exclusiveFilter: function (frame) {
		// remove empty tags
		return  (['a', 'div', 'p'].indexOf(frame.tag) && !frame.text.trim());
	},
	parser: {
		lowerCaseTags: true
	}
};



const cleanHTML = (data) => {

	let clean = sanitizeHtml (data, cleanOptions);

	// don't write empty files
	if ('' === clean.trim()) return console.log ('empty result');

	// save clean HTML to file
	let timestamp = Date.now();
	let outputFile = `${inputFile.replace('.html','')}-clean-${timestamp}.html`;

	fs.writeFile (outputFile, clean, function(err) {
		if(err) return console.log(err);
		console.log (`Clean file saved to "${outputFile}"`);
	});
};



const init = () => {

	// clean and save file
	fs.readFile (inputFile, 'utf8', (err, data) => {
		if (err) return console.log ('error', err);
		else return cleanHTML (data);
	});

};


init ();
